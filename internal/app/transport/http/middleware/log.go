package middleware

import (
	"context"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	logger "subscriber/internal/pkg/log"
	"time"
)

const (
	RequestIDCtxKey = "RequestID"
)

func RequestID() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		requestID := uuid.New().String()
		ctx.Locals(RequestIDCtxKey, requestID)
		return ctx.Next()
	}
}

//func RequestLogger(ctx context.Context) (fiber.Handler, error) {
//	log, err := logger.GetLogger(ctx)
//	if err != nil {
//		return nil, err
//	}
//}

func RequestInfo(ctx context.Context) (fiber.Handler, error) {
	log, err := logger.GetLogger(ctx)
	if err != nil {
		return nil, err
	}

	return func(ctx *fiber.Ctx) error {
		requestCallTime := time.Now()
		err := ctx.Next()

		log.Info(
			"request",
			"ip", ctx.IP(),
			"url", fmt.Sprintf("%s%s", ctx.BaseURL(), ctx.OriginalURL()),
			"duration", time.Since(requestCallTime).String(),
			"status_code", ctx.Response().StatusCode(),
			"error", err,
		)

		return nil
	}, nil
}
