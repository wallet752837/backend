package api

import (
	"context"
	"github.com/gofiber/fiber/v2"
	"subscriber/internal/app/models"
)

type UserOperationsService interface {
	CreateSubscription(ctx context.Context, arg models.CreateSubscriptionParams) (int64, error)
	CreateSubscriptions(ctx context.Context, arg []models.CreateSubscriptionParams) ([]int64, error)
	GetOperation(ctx context.Context, id int64) (models.UserOperation, error)
	GetOperations(ctx context.Context, arg models.GetOperationsParams) ([]models.UserOperation, error)
	GetSubscription(ctx context.Context, id int64) (models.Subscription, error)
	GetSubscriptions(ctx context.Context, arg models.GetSubscriptionsParams) ([]models.Subscription, error)
}

type Router struct {
	userOperations UserOperationsService
	server         *fiber.App
}

func New(userOperations UserOperationsService) *Router {
	return &Router{userOperations: userOperations, server: fiber.New()}
}

func (r *Router) Listen(addr string) error {
	r.server.Get("/operations", r.GetOperations)
	r.server.Get("/operations/:id", r.GetOperation)

	r.server.Get("/subscriptions", r.GetSubscriptions)
	r.server.Get("/subscriptions/:id", r.GetSubscription)

	r.server.Post("/subscriptions", r.CreateSubscriptions)
	return r.server.Listen(addr)
}

func (r *Router) Shutdown() error {
	return r.server.Shutdown()
}
