package api

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	"strconv"
	"subscriber/internal/app/models"
	"subscriber/internal/pkg/exterrors"
)

func (r *Router) GetOperations(c *fiber.Ctx) error {
	var params models.GetOperationsParams
	if err := c.QueryParser(&params); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
	}

	operations, err := r.userOperations.GetOperations(c.Context(), params)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(operations)
}

func (r *Router) GetOperation(c *fiber.Ctx) error {
	var id int
	var err error

	if id, err = strconv.Atoi(c.Params("id")); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
	}

	operation, err := r.userOperations.GetOperation(c.Context(), int64(id))
	if err != nil {
		if errors.Is(err, exterrors.ErrEntityNotFound) {
			return c.SendStatus(fiber.StatusNotFound)
		}
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(operation)
}

func (r *Router) CreateSubscriptions(c *fiber.Ctx) error {
	var params []models.CreateSubscriptionParams
	if err := c.BodyParser(&params); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
	}
	id, err := r.userOperations.CreateSubscriptions(c.Context(), params)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(id)
}

func (r *Router) GetSubscriptions(c *fiber.Ctx) error {
	var params models.GetSubscriptionsParams
	if err := c.QueryParser(&params); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
	}

	subscriptions, err := r.userOperations.GetSubscriptions(c.Context(), params)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(subscriptions)
}

func (r *Router) GetSubscription(c *fiber.Ctx) error {
	var id int
	var err error

	if id, err = strconv.Atoi(c.Params("id")); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
	}

	subscriptions, err := r.userOperations.GetSubscription(c.Context(), int64(id))
	if err != nil {
		if errors.Is(err, exterrors.ErrEntityNotFound) {
			return c.SendStatus(fiber.StatusNotFound)
		}
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(subscriptions)
}
