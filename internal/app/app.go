package app

import (
	"context"
	"database/sql"
	"fmt"
	"subscriber/internal/app/config"
	"subscriber/internal/app/migrations"
	"subscriber/internal/app/respository"
	"subscriber/internal/app/respository/user_operations"
	"subscriber/internal/app/transport/http/api"
	"subscriber/internal/app/usecase"
	"subscriber/internal/pkg/closer"
	logger "subscriber/internal/pkg/log"
)

func Run(ctx context.Context, cfg *config.Config) error {
	op := "app.Run"

	log, err := logger.New(cfg.LogLevel)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	ctx = context.WithValue(ctx, logger.LoggerCtxKey, log)

	db, err := sql.Open("postgres", cfg.Postgres.URL)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	err = migrations.Up(cfg.Postgres.URL)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	userOperationsRepo := user_operations.New(db)
	dto := respository.New(userOperationsRepo)
	service := usecase.NewUserOperationsService(dto)
	router := api.New(service)

	closer := closer.New()
	closer.Add(func(ctx context.Context) error {
		return router.Shutdown()
	})
	errorsC := make(chan error, 1)
	go func() {
		err := router.Listen(fmt.Sprintf(":%d", cfg.HTTPServer.Port))
		errorsC <- err
	}()

	select {
	case <-ctx.Done():
		log.Warn("context was canceled")
		return nil
	case err = <-errorsC:
		return err
	}
}
