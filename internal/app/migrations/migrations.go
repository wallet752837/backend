package migrations

import (
	"database/sql"
	"embed"
	"errors"
	"fmt"
	goose "github.com/pressly/goose/v3"
	"subscriber/internal/pkg/exterrors"

	_ "github.com/lib/pq"
)

//go:embed pg/*.sql
var embedMigrations embed.FS

func setupDB(conn string) (*sql.DB, error) {
	const op = "migrations.setup"

	goose.SetBaseFS(embedMigrations)
	err := goose.SetDialect("postgres")
	if err != nil {
		return nil, errors.Join(fmt.Errorf("%s: %w", op, err), exterrors.ErrDatabaseInternal)
	}
	db, err := goose.OpenDBWithDriver("postgres", conn)
	if err != nil {
		return nil, errors.Join(fmt.Errorf("%s: %w", op, err), exterrors.ErrDatabaseInternal)
	}

	return db, nil
}

func Up(conn string) error {
	const op = "migrations.Up"

	db, err := setupDB(conn)
	if err != nil {
		return err
	}

	defer db.Close()

	err = goose.Up(db, "pg")
	if err != nil {
		return errors.Join(fmt.Errorf("%s: %w", op, err), exterrors.ErrDatabaseInternal)
	}
	return nil
}

func Down(conn string) error {
	const op = "migrations.Down"

	db, err := setupDB(conn)
	if err != nil {
		return err
	}
	defer db.Close()

	err = goose.Down(db, "pg")
	if err != nil {
		return errors.Join(fmt.Errorf("%s: %w", op, err), exterrors.ErrDatabaseInternal)
	}
	return nil
}
