CREATE TABLE public.user_operations(
    id BIGSERIAL PRIMARY KEY,
    call_data TEXT NOT NULL,
    init_code TEXT NOT NULL,
    nonce TEXT NOT NULL,
    paymaster_and_data TEXT NOT NULL,
    call_gas_limit TEXT NOT NULL,
    sender TEXT NOT NULL,
    max_fee_per_gas TEXT NOT NULL,
    max_priority_fee_per_gas TEXT NOT NULL,
    pre_verification_gas TEXT NOT NULL,
    verification_gas_limit TEXT NOT NULL,
    signature TEXT NOT NULL
);

CREATE TYPE public.status AS ENUM (
    'not_sent',
    'pending',
    'confirmed',
    'failed'
);

CREATE TABLE public.subscriptions(
     id        BIGSERIAL PRIMARY KEY,
     operation_id           BIGINT NOT NULL REFERENCES public.user_operations(id),
     entrypoint             TEXT NOT NULL,
     send_at                TIMESTAMPTZ NOT NULL,
     status                 public.status NOT NULL DEFAULT 'not_sent',
     updated_at             TIMESTAMPTZ,
     created_at             TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX idx_operation_id ON public.subscriptions(operation_id);

CREATE OR REPLACE FUNCTION fn_update_timestamp()
    RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW();
RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_before_update_subscriptions
    BEFORE UPDATE ON public.subscriptions
    FOR EACH ROW
    EXECUTE FUNCTION fn_update_timestamp();