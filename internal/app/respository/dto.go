package respository

import (
	"context"
	"subscriber/internal/app/models"
	"subscriber/internal/app/respository/user_operations"
)

type UserOperations struct {
	queries *user_operations.Queries
}

func New(queries *user_operations.Queries) *UserOperations {
	return &UserOperations{queries: queries}
}

func (u UserOperations) CreateSubscription(ctx context.Context, arg models.CreateSubscriptionParams) (int64, error) {
	operationID, err := u.queries.CreateUserOperation(ctx, user_operations.CreateUserOperationParams{
		CallData:             arg.UserOperation.CallData,
		InitCode:             arg.UserOperation.InitCode,
		Nonce:                arg.UserOperation.Nonce,
		PaymasterAndData:     arg.UserOperation.PaymasterAndData,
		CallGasLimit:         arg.UserOperation.CallGasLimit,
		Sender:               arg.UserOperation.Sender,
		MaxFeePerGas:         arg.UserOperation.MaxFeePerGas,
		MaxPriorityFeePerGas: arg.UserOperation.MaxPriorityFeePerGas,
		PreVerificationGas:   arg.UserOperation.PreVerificationGas,
		VerificationGasLimit: arg.UserOperation.VerificationGasLimit,
		Signature:            arg.UserOperation.Signature,
	})
	if err != nil {
		return 0, err
	}

	return u.queries.CreateSubscription(ctx, user_operations.CreateSubscriptionParams{
		OperationID: operationID,
		Entrypoint:  arg.EntryPoint,
		Status:      user_operations.StatusNotSent,
		SendAt:      arg.SendAt,
	})
}

func (u UserOperations) CreateUserOperation(ctx context.Context, arg models.CreateUserOperationParams) (int64, error) {
	return u.queries.CreateUserOperation(ctx, user_operations.CreateUserOperationParams{
		CallData:             arg.CallData,
		InitCode:             arg.InitCode,
		Nonce:                arg.Nonce,
		PaymasterAndData:     arg.PaymasterAndData,
		CallGasLimit:         arg.CallGasLimit,
		Sender:               arg.Sender,
		MaxFeePerGas:         arg.MaxFeePerGas,
		MaxPriorityFeePerGas: arg.MaxPriorityFeePerGas,
		PreVerificationGas:   arg.PreVerificationGas,
		VerificationGasLimit: arg.VerificationGasLimit,
		Signature:            arg.Signature,
	})
}

func (u UserOperations) GetOperation(ctx context.Context, id int64) (models.UserOperation, error) {
	operation, err := u.queries.GetOperation(ctx, id)
	if err != nil {
		return models.UserOperation{}, err
	}

	return models.UserOperation{
		ID:                   operation.ID,
		CallData:             operation.CallData,
		InitCode:             operation.InitCode,
		Nonce:                operation.Nonce,
		PaymasterAndData:     operation.PaymasterAndData,
		CallGasLimit:         operation.CallGasLimit,
		Sender:               operation.Sender,
		MaxFeePerGas:         operation.MaxFeePerGas,
		MaxPriorityFeePerGas: operation.MaxPriorityFeePerGas,
		PreVerificationGas:   operation.PreVerificationGas,
		VerificationGasLimit: operation.VerificationGasLimit,
		Signature:            operation.Signature,
	}, nil
}

func (u UserOperations) GetOperations(ctx context.Context, arg models.GetOperationsParams) ([]models.UserOperation, error) {
	operations, err := u.queries.GetOperations(ctx, user_operations.GetOperationsParams{
		Limit:  arg.Limit,
		Offset: arg.Offset,
	})
	if err != nil {
		return nil, err
	}

	if len(operations) == 0 {
		return []models.UserOperation{}, nil
	}

	modelOperations := make([]models.UserOperation, len(operations))
	for i, operation := range operations {
		modelOperations[i] = models.UserOperation{
			ID:                   operation.ID,
			CallData:             operation.CallData,
			InitCode:             operation.InitCode,
			Nonce:                operation.Nonce,
			PaymasterAndData:     operation.PaymasterAndData,
			CallGasLimit:         operation.CallGasLimit,
			Sender:               operation.Sender,
			MaxFeePerGas:         operation.MaxFeePerGas,
			MaxPriorityFeePerGas: operation.MaxPriorityFeePerGas,
			PreVerificationGas:   operation.PreVerificationGas,
			VerificationGasLimit: operation.VerificationGasLimit,
			Signature:            operation.Signature,
		}
	}

	return modelOperations, nil
}

func (u UserOperations) GetSubscription(ctx context.Context, id int64) (models.Subscription, error) {
	sub, err := u.queries.GetSubscription(ctx, id)
	if err != nil {
		return models.Subscription{}, err
	}

	return models.Subscription{
		ID:          sub.ID,
		OperationID: sub.OperationID,
		Entrypoint:  sub.Entrypoint,
		SendAt:      sub.SendAt,
		Status:      models.Status(sub.Status),
		UpdatedAt:   sub.UpdatedAt,
		CreatedAt:   sub.CreatedAt,
	}, nil
}

func (u UserOperations) GetSubscriptions(ctx context.Context, arg models.GetSubscriptionsParams) ([]models.Subscription, error) {
	subs, err := u.queries.GetSubscriptions(ctx, user_operations.GetSubscriptionsParams{
		Limit:  arg.Limit,
		Offset: arg.Offset,
	})
	if err != nil {
		return nil, err
	}

	modelSubscriptions := make([]models.Subscription, len(subs))
	for i, sub := range subs {
		modelSubscriptions[i] = models.Subscription{
			ID:          sub.ID,
			OperationID: sub.OperationID,
			Entrypoint:  sub.Entrypoint,
			SendAt:      sub.SendAt,
			Status:      models.Status(sub.Status),
			UpdatedAt:   sub.UpdatedAt,
			CreatedAt:   sub.CreatedAt,
		}
	}

	return modelSubscriptions, nil
}

func (u UserOperations) UpdateSubscriptionStatus(ctx context.Context, arg models.UpdateSubscriptionStatusParams) error {
	return u.queries.UpdateSubscription(ctx, user_operations.UpdateSubscriptionParams{
		ID:     arg.ID,
		Status: user_operations.Status(arg.Status),
	})
}
