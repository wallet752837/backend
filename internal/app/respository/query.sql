-- name: CreateUserOperation :one
INSERT INTO public.user_operations (
    call_data,
    init_code,
    nonce,
    paymaster_and_data,
    call_gas_limit,
    sender,
    max_fee_per_gas,
    max_priority_fee_per_gas,
    pre_verification_gas,
    verification_gas_limit,
    signature
)
VALUES
(
    $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
)
RETURNING id;

-- name: GetOperation :one
SELECT
    id,
    call_data,
    init_code,
    nonce,
    paymaster_and_data,
    call_gas_limit,
    sender,
    max_fee_per_gas,
    max_priority_fee_per_gas,
    pre_verification_gas,
    verification_gas_limit,
    signature
FROM
    public.user_operations
WHERE
        id = $1;

-- name: GetOperations :many
SELECT
    id,
    call_data,
    init_code,
    nonce,
    paymaster_and_data,
    call_gas_limit,
    sender,
    max_fee_per_gas,
    max_priority_fee_per_gas,
    pre_verification_gas,
    verification_gas_limit,
    signature
FROM
    public.user_operations
LIMIT $1 OFFSET $2;

-- name: CreateSubscription :one
INSERT INTO public.subscriptions (
    operation_id,
    entrypoint,
    send_at,
    status
)
VALUES
    (
        $1, $2, $3, $4
    )
RETURNING id;

-- name: UpdateSubscription :exec
UPDATE public.subscriptions
SET
    status = $1
WHERE
        id = $2;

-- name: GetSubscription :one
SELECT
    id,
    operation_id,
    entrypoint,
    send_at,
    status,
    updated_at,
    created_at
FROM
    public.subscriptions
WHERE
        id = $1;

-- name: GetSubscriptions :many
SELECT
    id,
    operation_id,
    entrypoint,
    send_at,
    status,
    updated_at,
    created_at
FROM
    public.subscriptions
LIMIT $1 OFFSET $2;