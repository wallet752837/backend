package config

import (
	"fmt"
	env "github.com/caarlos0/env/v9"
	"subscriber/internal/pkg/exterrors"
)

type HTTPServer struct {
	Port int `env:"PORT" envDefault:"80"`
}

type Postgres struct {
	URL string `env:"URL,required"`
}

type Node struct {
	URL string `env:"URL,required"`
}

type Config struct {
	LogLevel   string     `env:"LOG_LEVEL" envDefault:"debug"`
	HTTPServer HTTPServer `envPrefix:"HTTP_SERVER_"`
	Node       Node       `envPrefix:"NODE_"`
	Postgres   Postgres   `envPrefix:"POSTGRES_"`
}

func Load() (*Config, error) {
	op := "config.Load"

	cfg := &Config{}
	err := env.Parse(cfg)
	if err != nil {
		return nil, fmt.Errorf("%s: %w: %w", op, exterrors.ErrFailedToLoadConfig, err)
	}
	return cfg, nil
}
