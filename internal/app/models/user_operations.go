package models

import (
	"database/sql"
	"time"
)

type Status string

const (
	StatusNotSent   Status = "not_sent"
	StatusPending   Status = "pending"
	StatusConfirmed Status = "confirmed"
	StatusFailed    Status = "failed"
)

type Subscription struct {
	ID          int64        `json:"id"`
	OperationID int64        `json:"operationId"`
	Entrypoint  string       `json:"entrypoint"`
	SendAt      time.Time    `json:"sendAt"`
	Status      Status       `json:"status"`
	UpdatedAt   sql.NullTime `json:"updatedAt"`
	CreatedAt   sql.NullTime `json:"createdAt"`
}

type UserOperation struct {
	ID                   int64  `json:"id"`
	CallData             string `json:"callData"`
	InitCode             string `json:"initCode"`
	Nonce                string `json:"nonce"`
	PaymasterAndData     string `json:"paymasterAndData"`
	CallGasLimit         string `json:"callGasLimit"`
	Sender               string `json:"sender"`
	MaxFeePerGas         string `json:"maxFeePerGas"`
	MaxPriorityFeePerGas string `json:"maxPriorityFeePerGas"`
	PreVerificationGas   string `json:"preVerificationGas"`
	VerificationGasLimit string `json:"verificationGasLimit"`
	Signature            string `json:"signature"`
}
