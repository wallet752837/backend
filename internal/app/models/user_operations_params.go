package models

import (
	"database/sql"
	"time"
)

type GetOperationsParams struct {
	Limit  int32 `query:"limit"`
	Offset int32 `query:"offset"`
}

type CreateUserOperationParams struct {
	CallData             string `json:"callData"`
	InitCode             string `json:"initCode"`
	Nonce                string `json:"nonce"`
	PaymasterAndData     string `json:"paymasterAndData"`
	CallGasLimit         string `json:"callGasLimit"`
	Sender               string `json:"sender"`
	MaxFeePerGas         string `json:"maxFeePerGas"`
	MaxPriorityFeePerGas string `json:"maxPriorityFeePerGas"`
	PreVerificationGas   string `json:"preVerificationGas"`
	VerificationGasLimit string `json:"verificationGasLimit"`
	Signature            string `json:"signature"`
}

type CreateSubscriptionParams struct {
	SendAt        int64                     `json:"sendAt"`
	UserOperation CreateUserOperationParams `json:"userOperation"`
	EntryPoint    string                    `json:"entryPoint"`
}

type CreateSubscriptionsParams struct {
	SendAt        time.Time                 `json:"sendAt"`
	UserOperation CreateUserOperationParams `json:"userOperation"`
	EntryPoint    string                    `json:"entryPoint"`
}

type GetSubscriptionParams struct {
	Id int64 `query:"id"`
}

type GetSubscriptionsParams struct {
	Limit  int32 `query:"limit"`
	Offset int32 `query:"offset"`
}

type UpdateSubscriptionStatusParams struct {
	SendAt    time.Time    `json:"sendAt"`
	Status    Status       `json:"status"`
	UpdatedAt sql.NullTime `json:"updatedAt"`
	ID        int64        `json:"id"`
}
