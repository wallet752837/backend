package usecase

import (
	"context"
	"subscriber/internal/app/models"
)

type UserOperationsRepo interface {
	CreateUserOperation(ctx context.Context, arg models.CreateUserOperationParams) (int64, error)
	GetOperation(ctx context.Context, id int64) (models.UserOperation, error)
	GetOperations(ctx context.Context, arg models.GetOperationsParams) ([]models.UserOperation, error)
	GetSubscription(ctx context.Context, id int64) (models.Subscription, error)
	GetSubscriptions(ctx context.Context, arg models.GetSubscriptionsParams) ([]models.Subscription, error)
	CreateSubscription(ctx context.Context, arg models.CreateSubscriptionParams) (int64, error)
	UpdateSubscriptionStatus(ctx context.Context, arg models.UpdateSubscriptionStatusParams) error
}

func (u UserOperationsService) CreateSubscription(ctx context.Context, arg models.CreateSubscriptionParams) (int64, error) {
	return u.userOperations.CreateSubscription(ctx, arg)
}

func (u UserOperationsService) CreateSubscriptions(ctx context.Context, subscriptions []models.CreateSubscriptionParams) ([]int64, error) {
	if len(subscriptions) == 0 {
		return []int64{}, nil
	}
	ids := make([]int64, 0, len(subscriptions))
	for _, sub := range subscriptions {
		id, err := u.userOperations.CreateSubscription(ctx, sub)
		if err != nil {
			return nil, err
		}
		ids = append(ids, id)
	}
	return ids, nil
}

func (u UserOperationsService) GetOperation(ctx context.Context, id int64) (models.UserOperation, error) {
	return u.userOperations.GetOperation(ctx, id)
}

func (u UserOperationsService) GetOperations(ctx context.Context, arg models.GetOperationsParams) ([]models.UserOperation, error) {
	return u.userOperations.GetOperations(ctx, arg)
}

func (u UserOperationsService) GetSubscription(ctx context.Context, id int64) (models.Subscription, error) {
	return u.userOperations.GetSubscription(ctx, id)
}

func (u UserOperationsService) GetSubscriptions(ctx context.Context, arg models.GetSubscriptionsParams) ([]models.Subscription, error) {
	return u.userOperations.GetSubscriptions(ctx, arg)
}

type UserOperationsService struct {
	userOperations UserOperationsRepo
}

func NewUserOperationsService(repo UserOperationsRepo) *UserOperationsService {
	return &UserOperationsService{userOperations: repo}
}
