package ethclient

type UserOperation struct {
	CallData             string `json:"callData"`
	InitCode             string `json:"initCode"`
	Nonce                string `json:"nonce"`
	PaymasterAndData     string `json:"paymasterAndData"`
	CallGasLimit         string `json:"callGasLimit"`
	Sender               string `json:"sender"`
	MaxFeePerGas         string `json:"maxFeePerGas"`
	MaxPriorityFeePerGas string `json:"maxPriorityFeePerGas"`
	PreVerificationGas   string `json:"preVerificationGas"`
	VerificationGasLimit string `json:"verificationGasLimit"`
	Signature            string `json:"signature"`
}
