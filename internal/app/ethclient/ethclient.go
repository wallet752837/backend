package ethclient

import (
	client "github.com/ethereum/go-ethereum/rpc"
)

type Client struct {
	url string
}

func New() {

}

func (c *Client) SendUserOperation(operation UserOperation) error {
	client, err := client.Dial(c.url)
	if err != nil {
		return err
	}

	var result string
	return client.Call(&result, "eth_sendUserOperation", operation)
}
