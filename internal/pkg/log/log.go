package log

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"subscriber/internal/pkg/exterrors"
)

type Level string

const (
	Debug Level = "debug"
	Info  Level = "info"
	Warn  Level = "warn"
	Error Level = "error"

	LoggerCtxKey = "logger"
)

type Logger struct {
	*slog.Logger
}

func GetLogger(ctx context.Context) (*Logger, error) {
	op := "log.GetLogger"

	value := ctx.Value(LoggerCtxKey)
	if value == nil {
		return nil, fmt.Errorf("%s: %w", op, exterrors.ErrLoggerNotDefined)
	}

	log, ok := value.(*Logger)
	if !ok {
		fmt.Errorf("%s: %w", op, exterrors.ErrLoggerTypeIsIncorrect)
	}
	return log, nil

}

func DefaultLogger() *Logger {
	return &Logger{
		slog.New(
			slog.NewJSONHandler(os.Stdout, nil),
		)}
}

func New(level string) (*Logger, error) {
	ctx := context.Background()
	slogLevel, err := parseLevel(ctx, level)
	if err != nil {
		return nil, err
	}

	handler := slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: slogLevel,
	})

	return &Logger{
		slog.New(handler),
	}, nil
}

func parseLevel(ctx context.Context, level string) (slog.Level, error) {
	var slogLevel slog.Level
	switch Level(level) {
	case Debug:
		slogLevel = slog.LevelDebug
	case Info:
		slogLevel = slog.LevelInfo
	case Warn:
		slogLevel = slog.LevelWarn
	case Error:
		slogLevel = slog.LevelError
	default:
		return slog.LevelDebug, exterrors.ErrFailedToParseLogLevel
	}
	return slogLevel, nil
}

func (l *Logger) Fatal(msg string, attributes ...any) {
	l.Error(msg, attributes...)
	os.Exit(1)
}
