package closer

import "context"

type Closer struct {
	closers []func(ctx context.Context) error
}

func New() *Closer {
	return &Closer{make([]func(ctx context.Context) error, 0)}
}

func (c *Closer) Add(f func(ctx context.Context) error) {
	c.closers = append(c.closers, f)
}

func (c *Closer) Close(ctx context.Context) error {
	for i := len(c.closers) - 1; i >= 0; i-- {
		err := c.closers[i](ctx)
		if err != nil {
			return err
		}
	}

	return nil
}
