package exterrors

import "errors"

var (
	ErrInvalidInput        = errors.New("invalid input")
	ErrFieldsValidation    = errors.New("fields validation")
	ErrDatabaseInternal    = errors.New("database internal error")
	ErrEntityAlreadyExists = errors.New("entity already exists")
	ErrEntityNotFound      = errors.New("entity not found")

	ErrFailedToLoadConfig    = errors.New("failed to load config")
	ErrFailedToParseLogLevel = errors.New("failed to parse log level")
	ErrLoggerNotDefined      = errors.New("logger is not defined")
	ErrLoggerTypeIsIncorrect = errors.New("logger type is incorrect")
)
