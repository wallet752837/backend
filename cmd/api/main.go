package main

import (
	"context"
	"os"
	"os/signal"
	"subscriber/internal/app"
	"subscriber/internal/app/config"
	logger "subscriber/internal/pkg/log"
)

func main() {
	log := logger.DefaultLogger()

	cfg, err := config.Load()
	if err != nil {
		log.Fatal("", "error", err)
	}

	ctx := context.Background()
	ctx, _ = signal.NotifyContext(ctx, os.Interrupt)

	err = app.Run(ctx, cfg)
	if err != nil {
		log.Fatal("failed to run app", "error", err)
	}
}
