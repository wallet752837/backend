# Build stage
FROM golang:1.21.1 AS builder
WORKDIR /build
# Copy go.mod and go.sum files
COPY go.mod go.sum ./
# Download dependencies
RUN go mod download
# Copy the source code
COPY . .
# Build the Go application
RUN go build -o backend ./cmd/api/main.go
RUN ls -la

ENTRYPOINT [ "./backend" ]
